from ctypes import *
class ADT_L1_1553_CDP(Structure):
        _fields_= [("NextPtr", c_uint32),
                   ("BMCount", c_uint32),
                   ("APIinfo", c_uint32),
                   ("Rsvd1", c_uint32),
                   ("Rsvd2", c_uint32),
                   ("MaskValue", c_uint32),
                   ("MaskCompare", c_uint32),
                   ("CDPControlWord", c_uint32),
                   ("CDPStatusWord", c_uint32),
                   ("TimeHigh", c_uint32),
                   ("TimeLow", c_uint32),
                   ("IMGap", c_uint32),
                   ("Rsvd3", c_uint32),
                   ("CMD1info", c_uint32),
                   ("CMD2info", c_uint32),
                   ("STS1info", c_uint32),
                   ("STS2info", c_uint32),
                   ("DATAinfo", c_uint32 * 32)]

class adt_l1_1553_bc_cb(Structure):
        __fields_= [
	 ("NextMsgNum", c_uint32),					
	 ("Retry", c_uint32),						
	 ("Csr", c_uint32),						
	 ("CMD1Info", c_uint32),					
	 ("CMD2Info", c_uint32),					
	 ("FrameTime", c_uint32),					
	 ("DelayTime", c_uint32),					
	 ("BranchMsgNum", c_uint32),					
	 ("StartFrame", c_uint32),	
	 ("StopFrame", c_uint32),	
	 ("FrameRepRate", c_uint32),	
	 ("MsgNum", c_uint32),
	 ("NumBuffers", c_uint32)
        ]
        
if __name__ == "__main__":
    ADT_L1 = cdll.LoadLibrary("./ADT_L1.so")
    DEVID = 0x00000000 | 0x00000000 | 0x00001000 | 0x00000000

    func=ADT_L1.ADT_L1_Error_to_String
    func.restype = c_char_p

    print(func(10))
    
    status = ADT_L1.ADT_L1_1553_InitDefault(DEVID, 10);
    res = func(status)
    print(res)

    print("Checking Protocol Engine (PE) and API versions . . . ");
    peVersion = c_uint16()
    l0Version = c_uint32()
    l1Version = c_uint32()
    get_ver  = ADT_L1.ADT_L1_GetVersionInfo
    res = get_ver(DEVID, byref(peVersion), byref(l0Version), byref(l1Version))
    res = func(res)
    print(res, peVersion.value, l0Version.value, l1Version.value)

    bc_init = ADT_L1.ADT_L1_1553_BC_Init(DEVID, 10, 1, 0)
    print(func(bc_init))
    bc_alloc = ADT_L1.ADT_L1_1553_BC_CB_CDPAllocate(DEVID, 0, 1)
    print(func(bc_alloc))
    
    myBCCB = adt_l1_1553_bc_cb()
    myBCCB.CMD1Info = 0x0820
    myBCCB.Csr = 0x04000000 | 0x00000002
    myBCCB.DelayTime = 1000
    myBCCB.NextMsgNum = 0xFFFFFFFF

    print(f"BCCB nmsg {myBCCB.NextMsgNum}")

    res = ADT_L1.ADT_L1_1553_BC_CB_Write(DEVID, 0, byref(myBCCB));
    print(func(res))

    myCdp = ADT_L1_1553_CDP()

    for i in range(len(myCdp.DATAinfo)):
        myCdp.DATAinfo[i] = 0x00001100 + i
    res = ADT_L1.ADT_L1_1553_BC_CB_CDPWrite(DEVID, 0, 0, byref(myCdp))
    func(res)

    res = ADT_L1.ADT_L1_1553_BC_Start(DEVID, 0)
    print(func(res))
    status = ADT_L1.ADT_L1_1553_BC_Stop(DEVID)    

    isRunning = c_uint32(1)
    count = c_uint32(0)
    while isRunning:
        status = ADT_L1.ADT_L1_1553_BC_IsRunning(DEVID, byref(isRunning))
        ADT_L1.ADT_L1_1553_BC_GetFrameCount(DEVID, byref(count))
       # print(count, isRunning)
    print("BC stopped.")

    res = ADT_L1.ADT_L1_1553_BC_CB_CDPRead(DEVID, 0, 0, byref(myCdp))
    print(myCdp.STS1info)

    ADT_L1.ADT_L1_1553_BC_Close(DEVID)
    ADT_L1.ADT_L1_CloseDevice(DEVID)

# Note to self: Alta Passive Monitor Protocol (APMP)
# In addition to ADCP devive control mode, ENET devices can be optionally
# configured to simultaneously broadcast Ethernet packets containing 1553 or
# ARINC receive data. This uses the Alta Passive Monitor Protocol (APMP) and
# details are provided at the end of this section. Essentially, you can setup a
# unique UDP packet on the ENET device to auto transmit 1553/ARINC packets as
# they arrive so that any computer on the LAN can monitor or record data. There
# are also several example programs you can cut/paste to implement a simple
# UDP APMP monitor. This requires VERY little code and could be done in almost
# any language or computer system.
    
#Note, ADT_L0_CALL_CONV expands to empty string, it is not needed in the python header

#Note, development is a little slower, but actually more concise.
# Since ADT_L0_UINT32 is a fundamental type for almost every function call, this shouldn't get too messy.

