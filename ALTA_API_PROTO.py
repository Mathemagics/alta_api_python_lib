from cffi import FFI
ffibuilder_L0 = FFI()


#defines, typedefs, function protos, (structs?) you want to export
ffibuilder_L0.cdef("""
                      #define ADT_L0_API_VERSION ...
                   """)

ffibuilder_L0.set_source("ADT_L0",  # name of the output C extension. #Includes in raw string. 
"""
    #include "ADT_L0.h"
""",
    sources=["ADT_L0_BSD_ENET.c"]
    #sources you would need to be able to compile
    #libraries=['m']    #additional libs
                      )

################################


ffibuilder_L1 = FFI()
#defines, typedefs, function protos, (structs?) you want to export, In reality, you can make a new header that doesn't contain directives, and call open(file.h).read(), this might be cleaner.
ffibuilder_L1.cdef("""
                   #define ADT_L1_API_VERSION ...
                   #define ADT_DEVID_BOARDTYPE_SIM1553 ...
                   #define ADT_PRODUCT_SIM1553 ...
                   #define ADT_L1_API_DEVICEINIT_FORCEINIT ...
                   #define ADT_L1_API_DEVICEINIT_ROOTPERESET ...
                   #define ADT_SUCCESS ...
                   #define ADT_ERR_BAD_INPUT ...
                   #define ADT_FAILURE ...
                   #define ADT_DEVID_BOARDNUM_01 ...
                   #define ADT_DEVID_CHANNELTYPE_1553 ...
                   #define ADT_DEVID_CHANNELNUM_01 ...
                   typedef unsigned int ADT_L0_UINT32;
                   typedef unsigned short ADT_L0_UINT16;
                   ADT_L0_UINT32 ADT_L1_InitMemMgmt(ADT_L0_UINT32 devID, ADT_L0_UINT32 startpOptions);
                   char * ADT_L1_Error_to_String(ADT_L0_UINT32 err_status);
                   ADT_L0_UINT32 ADT_L1_1553_InitDefault_ExtendedOptions(ADT_L0_UINT32 devID, ADT_L0_UINT32 numIQEntries, ADT_L0_UINT32 startupOptions);
                   ADT_L0_UINT32 ADT_L1_1553_InitDefault(ADT_L0_UINT32 devID, ADT_L0_UINT32 numIQEntries);
                   ADT_L0_UINT32 ADT_L1_GetVersionInfo(ADT_L0_UINT32 devID,
									ADT_L0_UINT16 *peVersion,
									ADT_L0_UINT32 *layer0ApiVersion,
									ADT_L0_UINT32 *layer1ApiVersion);
                   """)

ffibuilder_L1.set_source("ADT_L1",  # name of the output C extension. #Includes in raw string. 
"""
    #include "ADT_L1.h"
""",#sources: you would need to be able to compile
    sources=["ADT_L1_1553_General.c",
             #"ADT_L1_A429_SG.c",
             "ADT_L1_BoardGlobal.c",
             "ADT_L1_BIT.c",
             "ADT_L1_MemMgmt.c",
             "ADT_L1_1553_RT.c",
             "ADT_L1_1553_BC.c",
             "ADT_L1_1553_INT.c",
             "ADT_L1_1553_PB.c",
             "ADT_L1_INT.c",
             #"ADT_L1_A429_INT.c",
             #"ADT_L1_A429_PB.c",
             "ADT_L1_1553_BM.c",
             #"ADT_L1_A429_RX_MC.c",
             #"ADT_L1_A429_TX.c",
             "ADT_L1_General.c",
             #"ADT_L1_A429_General.c",
             #"ADT_L1_A429_RX.c",
             "ADT_L0_BSD_ENET.c",
             "ADT_L1_1553_ex_bcbm1_Timing_Test.c",
             "ADT_L1_1553_SG.c"]

    #libraries=['m']    #additional libs
                      )

if __name__ == "__main__":
    ffibuilder_L0.compile(verbose=False)
    ffibuilder_L1.compile(verbose=False)
#    from ADT_L1.lib import ADT_L1_API_VERSION, ADT_DEVID_BOARDTYPE_SIM1553, ADT_L1_1553_InitDefault_ExtendedOptions, ADT_L1_API_DEVICEINIT_FORCEINIT, ADT_L1_API_DEVICEINIT_ROOTPERESET, ADT_SUCCESS, ADT_ERR_BAD_INPUT, ADT_FAILURE, ADT_PRODUCT_SIM1553, ADT_L1_1553_InitDefault, ADT_DEVID_BOARDNUM_01, ADT_DEVID_CHANNELTYPE_1553 , ADT_DEVID_CHANNELNUM_01

    # This would be a new file, since we've already compiled the .so
    import ADT_L1 as ADT_L1
    #import ADT_L0 as ADT_L0
    from cffi import FFI
    
    print(ADT_L1_API_VERSION)
    DEVID = ADT_L1.lib.ADT_PRODUCT_SIM1553 | ADT_L1.lib.ADT_DEVID_BOARDNUM_01 |  ADT_L1.lib.ADT_DEVID_CHANNELTYPE_1553 | ADT_L1.lib.ADT_DEVID_CHANNELNUM_01
    status = ADT_L1.lib.ADT_L1_1553_InitDefault(DEVID, 10);

    if status == ADT_L1.lib.ADT_SUCCESS:
        print("YES!")
    elif status == ADT_L1.lib.ADT_ERR_BAD_INPUT:
        print("BAD INPUT!")
        print(status)
    elif status == ADT_L1.lib.ADT_FAILURE:
        print("EPIC FAIL!")
        print(status)
    else:
        print(f"Got {status} but wanted {ADT_SUCCESS}!")


    print("Checking Protocol Engine (PE) and API versions . . . ");
    peVersion = ADT_L1.ffi.new('ADT_L0_UINT16 *')
    l0Version = ADT_L1.ffi.new('ADT_L0_UINT32 *')
    l1Version = ADT_L1.ffi.new('ADT_L0_UINT32 *')
    status = ADT_L1.lib.ADT_L1_GetVersionInfo(DEVID, peVersion, l0Version, l1Version);
    print(peVersion[0], l0Version[0], l1Version[0]) #[0] to dereference pointer, x.y also works
    #help(ADT_L1)

# Note to self: Alta Passive Monitor Protocol (APMP)
# In addition to ADCP devive control mode, ENET devices can be optionally
# configured to simultaneously broadcast Ethernet packets containing 1553 or
# ARINC receive data. This uses the Alta Passive Monitor Protocol (APMP) and
# details are provided at the end of this section. Essentially, you can setup a
# unique UDP packet on the ENET device to auto transmit 1553/ARINC packets as
# they arrive so that any computer on the LAN can monitor or record data. There
# are also several example programs you can cut/paste to implement a simple
# UDP APMP monitor. This requires VERY little code and could be done in almost
# any language or computer system.
    
#Note, ADT_L0_CALL_CONV expands to empty string, it is not needed in the python header
